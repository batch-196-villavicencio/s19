console.log("Hello World");

let username = prompt("Enter your username:");
let password = prompt("Enter your password:");
let role = prompt("Enter your role:");

if (role !== null && role !== ""){
	role = role.toLowerCase();
}

if (username === null || !username.trim() || password === null || !password.trim() || role === null || !role.trim()) {
	alert("Input should not be empty");
} else {
	switch(role){
		case 'admin' :
		alert("Welcome back to the class portal, admin!");
		break;
		case 'teacher' :
		alert("Thank you for logging in, teacher!");
		break;
		case 'student' :
		alert("Welcome to the class portal, student!");
		break;
		default:
		alert("Role out of range");
		break;
	}
}

function checkAverage(num1,num2,num3,num4){
	let average = Math.round((num1+num2+num3+num4)/4);
	if (average <= 74) {
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is F");
	} else if (average >= 75 && average <= 79) {
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is D");
	} else if (average >= 80 && average <= 84) {
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is C");
	} else if (average >= 85 && average <= 89) {
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is B");
	} else if (average >= 90 && average <= 95) {
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is A");
	} else if (average > 95) {
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is A+");
	}
}
